from fastapi import APIRouter
from app.clients.dao import ClientsDAO
from app.clients.schema import SClient

from app.nps.dao import NpsDAO
from app.operators.schema import SNpsStatus


router =  APIRouter(
    prefix="/operators",
    tags=["Админка"],
)


@router.get("/get_not_accepted_nps/")
async def get_not_accepted_nps():
    return await NpsDAO.find_all(status='Considered')


@router.get("/get_client/{phone}")
async def get_client(phone: str):
    return await ClientsDAO.find_by_phone(phone=phone)


@router.patch("/put_status/")
async def patch_status(status: SNpsStatus):
    return await NpsDAO.patched_status_agree(status)
    