from pydantic import BaseModel


class SNpsStatus(BaseModel):
    id: int
    status: str