from sqlalchemy import Column, Integer, ForeignKey
from app.database import Base

class History(Base):
    __tablename__='history'

    id = Column(Integer, primary_key=True)
    phone = Column(ForeignKey("clients.phone"))
    nps_id = Column(ForeignKey("nps.id"))
    balance_changed = Column(Integer)
    