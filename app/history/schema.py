from pydantic import BaseModel
from datetime import date

class SHistory(BaseModel):
    phone: str
    nps_id: int
    balance_change: int