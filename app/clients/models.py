from sqlalchemy import Boolean, Column, Integer, Date, String, text
from app.database import Base

class Clients(Base):
    __tablename__='clients'

    name = Column(String, nullable=False)
    phone = Column(String, primary_key=True)
    e_mail = Column(String)
    date_birthday = Column(Date)
    balance = Column(Integer,server_default=text('0'))
    registered = Column(Date, nullable=False)
    consent_mailing = Column(Boolean, nullable=False)

    