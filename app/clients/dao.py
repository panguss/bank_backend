from app.database import async_session_maker
from app.clients.models import Clients
from app.dao.base import BaseDAO
from app.database import engine
from sqlalchemy import insert
from app.clients.models import Clients

from app.database import async_session_maker
from app.nps.models import Nps
from app.dao.base import BaseDAO
from app.database import engine


class ClientsDAO(BaseDAO):

    model = Clients
    
    @classmethod
    async def add_new_сlient(cls, nps):

        async with async_session_maker() as session:

            add_client = insert(Clients).values(
                            name = nps.name,
                            phone = nps.phone,
                            e_mail = nps.email,
                            date_birthday = nps.date_birthday,
                            registered = nps.datetime_quest,
                            consent_mailing = nps.consent_mailing
                        ).returning(Clients)
            
            new_client = await session.execute(add_client)  
            new_client = new_client.scalar_one_or_none()
            await session.commit()
            return new_client