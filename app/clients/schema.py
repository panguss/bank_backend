from pydantic import BaseModel
from datetime import date

class SClient(BaseModel):
    phone: str
    email: str
    date_birthday: date
    balance: int
    name: str
    registered: date