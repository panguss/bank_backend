import datetime
from pydantic import BaseModel, validator
from datetime import date
import re

from exception import IncorrectPhoneException


class SNpsNonReg(BaseModel):
    phone: str
    email: str
    datetime_quest: datetime.datetime
    consent_mailing: bool
    number_quest: str
    where_info: str 
    date_birthday: date
    name: str
    quest: str
    mark: int
    cause_mark: str
    wish: str

    @validator('phone', check_fields=False)
    def validate_phone_number(cls, v):
        if not re.match(r'^8\d{10}$',  v):
            raise IncorrectPhoneException

        return v

class SNpsReg(BaseModel):
    phone: str
    datetime_quest: datetime.datetime
    quest: str
    mark: int
    cause_mark: str
    wish: str

    @validator('phone', check_fields=False)
    def validate_phone_number(cls, v):
        if not re.match(r'^8\d{10}$', v):
            raise IncorrectPhoneException
            
        return v
    
    


        
 
