from sqlalchemy import insert, select, update
from app.clients.models import Clients
from app.clients.dao import ClientsDAO
from app.database import async_session_maker
from app.history.models import History
from app.nps.models import Nps
from app.dao.base import BaseDAO
from app.database import engine
from app.nps.schema import SNpsNonReg, SNpsReg
from sqlalchemy.sql import select

from app.operators.schema import SNpsStatus
from exception import UserAlreadyExistsException, UserNotExistsException

class NpsDAO(BaseDAO):

    model = Nps

    @classmethod
    async def add_registered(
        cls, nps: SNpsReg
    ):
        async with async_session_maker() as session:

            query = select(Clients).filter_by(phone=nps.phone)
            result = await session.execute(query)
            client = result.scalar_one_or_none()

            if client is not None and client.phone == nps.phone:
                add_nps = insert(Nps).values(
                    phone = client.phone,
                    quest = nps.quest,
                    datetime_quest = nps.datetime_quest,
                    mark = nps.mark,
                    cause_mark = nps.cause_mark, 
                    wish = nps.wish,
                    status = 'Considered'
                ).returning(Nps)
                new_nps = await session.execute(add_nps)
                await session.commit()
                return new_nps.scalar_one_or_none
                
            else:
                raise UserNotExistsException


    @classmethod          
    async def add_non_registered(
        cls, nps: SNpsNonReg
    ):
        async with async_session_maker() as session:

            query = select(Clients).filter_by(phone=nps.phone)
            result = await session.execute(query)
            client = result.scalar_one_or_none()

            if client is not None and client.phone == nps.phone:
                raise UserAlreadyExistsException

            else:
                
                new_client = await ClientsDAO.add_new_сlient(nps)

                add_nps = insert(Nps).values(
                    phone = new_client.phone,
                    quest = nps.quest,
                    datetime_quest = nps.datetime_quest,
                    mark = nps.mark,
                    cause_mark = nps.cause_mark, 
                    wish = nps.wish,
                    status = 'Considered'
                    ).returning(Nps)
                
                new_nps = await session.execute(add_nps)
                await session.commit()  
                return new_nps.scalar_one_or_none, new_client
            

    @classmethod
    async def patched_status_agree(
        cls, status: SNpsStatus
        ):
        async with async_session_maker() as session:

            puted_nps = (
                update(Nps)
                .where(
                        Nps.id == status.id
                    )
                .values(
                    status = status.status
                    )
                )
            
            puted_status = await session.execute(puted_nps)
            await session.commit()

            select_nps = (
                select(Nps)
                .where(Nps.id == status.id)
            )
            
            result = await session.execute(select_nps)
            query = result.scalar()

            add_history = (
                insert(History)
                .values(
                phone = query.phone,
                nps_id = query.id,
                balance_changed = 0
                )
            )
            added_history = await session.execute(add_history)
            await session.commit()
            return puted_status, added_history