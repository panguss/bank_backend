from fastapi import APIRouter

from app.nps.dao import NpsDAO
from app.nps.schema import SNpsNonReg, SNpsReg


router =  APIRouter(
    prefix="/nps",
    tags=["Анкета"],
)


@router.post("/add_reg_nps/")
async def add_nps(nps: SNpsReg):
    return await NpsDAO.add_registered(nps)


@router.post("/add_non_reg_nps/")
async def add_nps(nps: SNpsNonReg):
    return await NpsDAO.add_non_registered(nps)
    


