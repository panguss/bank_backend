from sqlalchemy import TIMESTAMP, Column, Integer, ForeignKey, Date, String
from app.database import Base

class Nps(Base):
    __tablename__='nps'
    
    id = Column(Integer, primary_key=True)
    phone = Column(ForeignKey("clients.phone"))
    quest = Column(String, nullable=False)
    datetime_quest = Column(TIMESTAMP(timezone=True), nullable=False)
    mark = Column(Integer, nullable=False)
    cause_mark = Column (String)
    wish = Column(String)
    cost = Column(Integer)
    diskount = Column(Integer)
    status = Column(String)
    when_add = Column(Date)
    when_check = Column(Date)
    who_check = Column(String)
    number_person = Column(Integer)

