"""new models

Revision ID: 144a3fdbef21
Revises: 
Create Date: 2023-08-18 15:31:15.643520

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '144a3fdbef21'
down_revision = None
branch_labels = None
depends_on = None


def upgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('clients',
    sa.Column('name', sa.String(), nullable=False),
    sa.Column('phone', sa.String(), nullable=False),
    sa.Column('e_mail', sa.String(), nullable=True),
    sa.Column('date_birthday', sa.Date(), nullable=True),
    sa.Column('balance', sa.Integer(), server_default=sa.text('0'), nullable=True),
    sa.Column('registered', sa.Date(), nullable=False),
    sa.Column('consent_mailing', sa.Boolean(), nullable=False),
    sa.PrimaryKeyConstraint('phone')
    )
    op.create_table('nps',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('phone', sa.String(), nullable=True),
    sa.Column('quest', sa.String(), nullable=False),
    sa.Column('datetime_quest', sa.DateTime(), nullable=False),
    sa.Column('mark', sa.Integer(), nullable=False),
    sa.Column('cause_mark', sa.String(), nullable=True),
    sa.Column('wish', sa.String(), nullable=True),
    sa.Column('cost', sa.Integer(), nullable=True),
    sa.Column('diskount', sa.Integer(), nullable=True),
    sa.Column('status', sa.String(), nullable=True),
    sa.Column('when_add', sa.Date(), nullable=True),
    sa.Column('when_check', sa.Date(), nullable=True),
    sa.Column('who_check', sa.String(), nullable=True),
    sa.Column('number_person', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['phone'], ['clients.phone'], ),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_table('history',
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('phone', sa.String(), nullable=True),
    sa.Column('nps_id', sa.Integer(), nullable=True),
    sa.Column('balance_changed', sa.Integer(), nullable=True),
    sa.ForeignKeyConstraint(['nps_id'], ['nps.id'], ),
    sa.ForeignKeyConstraint(['phone'], ['clients.phone'], ),
    sa.PrimaryKeyConstraint('id')
    )
    # ### end Alembic commands ###


def downgrade() -> None:
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_table('history')
    op.drop_table('nps')
    op.drop_table('clients')
    # ### end Alembic commands ###
