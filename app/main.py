from fastapi import FastAPI

from app.nps.router import router as router_nps
from app.operators.router import router as router_operators

from fastapi.middleware.cors import CORSMiddleware


app = FastAPI()

app.include_router(router_nps)

app.include_router(router_operators)


origins = [
    # 3000 - порт, на котором работает фронтенд на React.js
    "http://127.0.0.1:5173",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["GET", "POST", "OPTIONS", "DELETE", "PATCH", "PUT"],
    allow_headers=[
        "Content-Type",
        "Set-Cookie",
        "Access-Control-Allow-Headers",
        "Access-Control-Allow-Origin",
        "Authorization",
    ],
)
