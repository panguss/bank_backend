from fastapi import HTTPException, status


UserAlreadyExistsException = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Пользователь уже существует"
)


UserNotExistsException = HTTPException(
    status_code=status.HTTP_409_CONFLICT,
    detail="Пользователь не существует"
)


IncorrectPhoneException = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Неверный формат номера"
)





